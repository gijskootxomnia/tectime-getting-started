## Overview

week | topic | notebooks
--- | --- | ---
1 | Tools and project structure | 1, 2, 3
2 | Pandas, reading data and cleaning up | 4, 5
3 | Joining data, the index, and exercise in cleaning up | 6
4 | Visualization and exploration with `pandas` | 7
5 | Regression with `statsmodels` | 8
6 | Final exercise | 9, 10

### Notebooks

which | what
--- | ---
1_introduction | What is this series about
2_tools | What are `git`, `python`, `jupyter-notebook` and the command line, and why should you know?
3_project_organization | Sample directory structure and some remarks on keeping things organized
4_reading_data | How to read data with `pandas`, and have a look at the data
5_cleaning_up | How to find find and deal with missing values
6_join | How to join data, groupby and cleanup steps
7_visualisation | Quickly summarize and visualize data
8_regression | How to use `pd.get_dummies` and `sklearn` to fit a linear regression
9_conclusion | What are your conclusions from the exercise
10_exercise | A final exercise combining the earlier steps
hello
